package com.zonedldb.Utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ghyslainbruno on 22/08/2016.
 */
public class TmdbCalls {

    private String currentMovieTitle;
    private String currentMovieYear;
    private String apiKey = "7d7d89a7c475b8fdc9a5203419cb3964";
    private JsonObject tmdbResponse;
    public static boolean useYear;

    public TmdbCalls(String currentMovieTitle, String currentMovieYear) {
        this.currentMovieTitle = currentMovieTitle.replaceAll(" ","%20").replaceAll("#","");
        this.currentMovieYear = currentMovieYear.replaceAll(" ","%20");
    }

    // TODO: get the title from tmdb, to prevent cases when people made mistakes uploading on zone téléchargement (maybe year also)

    public String getId() {
        return tmdbResponse.get("results").getAsJsonArray().get(0).getAsJsonObject().get("id").getAsString();
    }

    public String getAverageNote() {
        return tmdbResponse.get("results").getAsJsonArray().get(0).getAsJsonObject().get("vote_average").getAsString();
    }

    public String getPosterPath() {
        return tmdbResponse.get("results").getAsJsonArray().get(0).getAsJsonObject().get("poster_path").getAsString();
    }

    public String getTitle() {
        return tmdbResponse.get("results").getAsJsonArray().get(0).getAsJsonObject().get("title").getAsString();
    }

    public String getReleaseDate() {
        return tmdbResponse.get("results").getAsJsonArray().get(0).getAsJsonObject().get("release_date").getAsString();
    }

    public void load() {
        try {
            String urlSearchBase = "http://api.themoviedb.org/3/search/multi?query=" + currentMovieTitle + "&api_key=" + apiKey + "&language=fr";
            if (useYear) {
                urlSearchBase = "http://api.themoviedb.org/3/search/multi?query=" + currentMovieTitle + "&api_key=" + apiKey + "&year=" + currentMovieYear + "&language=fr";
            }
            URL url = new URL(urlSearchBase);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream stream = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            tmdbResponse = new Gson().fromJson(reader, JsonObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isThereAResult() {
        int totalResults = tmdbResponse.get("total_results").getAsInt();
        if (totalResults == 0) {
            return false;
        }
        return true;
    }

}
